<?php require_once "includes/databases_conn.php"; ?>
<?php require_once "includes/functions.php"; ?>
<!DOCTYPE html>
<html>
<head>
      <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
      <link type="text/css" rel="stylesheet" href="css/custom.css"  media="screen,projection"/>
      <link href='https://fonts.googleapis.com/css?family=Rancho' rel='stylesheet' type='text/css'>
      <link href='https://fonts.googleapis.com/css?family=Poiret+One&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
     <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

      <script src='https://www.google.com/recaptcha/api.js'></script>
      <script type="text/javascript" src="js/custom.js"></script>

      <?php// include 'modules/opengraph.php'; ?>

</head>
<?php 
$result = mysqli_query($connection, "SELECT * from people");
$result2 = mysqli_query($connection, "SELECT * from messages");

while ($row = mysqli_fetch_assoc($result)) 
{
    if ($row["date"]==date("d-m",$t)) 
    {
      $semiuser = $row["username"];
      include "modules/ifbday.php";
      $check = 1;
      break;
    }
    else {
      $check = 0;
    }
}

if ($check==0)
{
  include "modules/nobday.php";
}
include "modules/footer.php";
?>

</body>
</html>
