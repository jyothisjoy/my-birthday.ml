<footer class="footerbg white-text">
<?php if (isset($connection)) { mysqli_close($connection); }?>
  <div class="section container">
   <div class="row">
    <div class="col l4 m4 s12">
     <p>&copy; 2015 to the end of time.<br><a target="_blank" href="">7410N Personal Project Initiative.</a> 
     <br> Built out of love and more love. Supported by a bunch of friends.</p>
    </div>
    <div class="col s12 m8 l8">
     <div class="row">
       <div class="col l4">
        <a href="http://jyothisjoy.com/" target="_blank"><img class="responsive-image circle" src="images/makers/joe.jpg" width="100px"></a>
        <h5>Jyothis Joy</h5>
        <p>The mechanic</p>
       </div>
       <div class="col l8">
        <a href="https://www.facebook.com/pritikammu?fref=ts" target="_blank"><img class="responsive-image circle" src="images/makers/pritika.jpg" width="100px"></a>
        <h5>Pritika Merryl</h5>
        <p>You know one of those people whom companies pay for ideas? except that this one dosen't get paid.</p>
       </div>
     </div>
    </div>
   </div>
   <div class="row">
     <h5>Usage Policy</h5>
     <p>This website is built in love for everyone. Anyone in the whole world wide web can use it. Best part, all this code is Open Source.<br>Hack and contribute <a target="_blank" href="https://bitbucket.org/jyothisjoy/jacob-george/">here.</a></p>
   </div>
   <div class="row">
     <h5>Privacy</h5>
     <p>This website collect email id for retrieving pictures using gravatar. You will not receive mail from this website
     <br> We hate spams as much as you do.<br>Want your image to show up? Register your email ID at <a target="_blank" href="http://gravatar.com">Globaly Recognized Avatars(Gravatar)</a></p>
   </div>
  </div>
</footer>
<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>

<script type="text/javascript">

  $(document).ready(function(){
   $('.parallax').parallax();});


  $(document).ready(function(){
   $('.scrollspy').scrollSpy();});


</script>