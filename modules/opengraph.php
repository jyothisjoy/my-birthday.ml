<!-- Open Graph -->
<meta name="og:title" content="Remembering Jacob George"/>
<meta name="og:type" content="Personal Website"/>
<meta name="og:url" content="http://www.reminiscejacob.in/"/>
<meta name="og:image" content="images/meta.jpg"/>
<meta name="og:site_name" content="Jacob George"/>
<meta name="og:description" content="Memorial Website for Jacob George aka Chacohi,our dearset friend."/>

<!-- Apple Cards -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta content="yes" name="apple-touch-fullscreen" />
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">

<!-- Schema.org markup for Google+ -->
<meta itemprop="name" content="Remembering Jacob George">
<meta name="keywords" content="Remembering, Jacob George, Jacob, George, Website, memorial">
<meta itemprop="description" content="Memorial Website for Jacob George aka Chacohi,our dearset friend.">
<meta itemprop="image" content="images/meta.jpg">

<!-- Twitter Card data -->
<meta name="twitter:card" content="logo.png">
<meta name="twitter:site" content="http://reminiscejacob.in">
<meta name="twitter:title" content="Teezonic Creative Studio">
<meta name="twitter:description" content="Memorial Website for Jacob George aka Chacohi,our dearset friend.">
<meta name="twitter:creator" content="@teezonic">
<!-- Twitter summary card with large image must be at least 280x150px -->
<meta name="twitter:image:src" content="images/meta.jpg">

<!--  HTML Meta Tags  -->
<meta name="description" content="Memorial Website for Jacob George aka Chacohi,our dearset friend.">
<link rel="shortcut icon" href="/images/favicon.png" />
<link rel="shortcut icon" type="image/png" href="images/favicon.png" />
<link rel="fluid-icon" type="image/png" href="images/favicon.png" />