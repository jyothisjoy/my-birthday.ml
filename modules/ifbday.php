<title><?php echo($row["name"]);?> | Happy Birthday!</title>
<!--  HTML Meta Tags  -->
<meta name="description" <?php  echo 'content="Happy Birthday. May this year be a huge success to you!"' ?>>
<link rel="shortcut icon" <?php  echo 'href="images/people/'.$row["username"].'.jpg"' ?> />
<link rel="shortcut icon" type="image/jpg" <?php  echo 'href="images/people/'.$row["username"].'.jpg"' ?> />
<link rel="fluid-icon" type="image/jpg" <?php  echo 'href="images/people/'.$row["username"].'.jpg"' ?> />
<body>
        <div class="parallax-container bg1 valign-wrapper center-align">
          <div class="row">
            <div class="col s12 wrapforhead">
              <h2>Happy Birthday!</h2>
            </div>
            <div class="col s12 wrapforhead">
              <h3><?php echo($row["name"]);?></h3>
            </div>
            <div class="col s12 wrapforpic">
              <img <?php  echo 'src="images/people/'.$row["username"].'.jpg"' ?> width="200px" class="circle responsive-img">
            </div>
          </div>
        </div>

        <div class="section white">
          <div class="row container">
            <h2 id="write" class="scrollspy">Wish <?php echo($row["name"]);?> Birthday!</h2>
          </div>
          <div class="row container">
            <form class="col s12" action="create_pyr.php" method="POST" name="myForm" onsubmit="return(validate());">
              <div class="row">
                <div class="input-field col s12">
                  <input placeholder="Name" id="name" name="Name" type="text" class="validate">
                  <label for="name">Name</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input id="email" placeholder="Email" name="EMail" type="email" class="validate">
                  <label for="email">Email (Will not be published)</label>
                </div>
              </div>
              <input id="reluser" type="hidden" name="REluser"  <?php  echo 'value="'.$row["username"].'"' ?>>
              <div class="row">
                <div class="input-field col s12">
                    <textarea id="textarea1" name="MEssage" placeholder="What's in your mind?" class="materialize-textarea"></textarea>
                    <label for="last_name">Whats In your Mind?</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                    <div class="g-recaptcha" data-sitekey="6Lfpcg4TAAAAALO3pYspkNY1Hg2Vds2R05WTAZ5l"></div>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                    <input type="submit" class="btn red" value="SHARE" name="submit">
                </div>
              </div>
            </form>
          </div>
        </div>


        <div class="section custborder white">
          <div class="row container">
                <?php while ($row2 = mysqli_fetch_assoc($result2)) { ?>
                 <?php  
                    if ($row["username"]==$row2["reluser"]) {
                  ?>
                  <div class="card scrollspy" <?php  echo 'id="'.$row2["name"].'"' ?> >
                    <div class="row card-content">
                      <div class="col s10">
                          <div class="card-title red-text"><?php echo $row2["name"]. "<br>"; ?></div>
                          <?php echo $row2["comment"]. "<br>";  ?>
                      </div>
                      <div class="col s2">

                        <?php
                          $randnumber = rand(1,7);
                          $email = $row2["email"];
                          $default = 'http://jyothisjoy.com/wp-content/uploads/2015/11/minion'.$randnumber.'.jpg';
                          $size = 100;
                          $grav_url = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;
                        ?>
                         <img src="<?php echo $grav_url; ?>" class="responsive-img circle" width="100px" height="100px">
                      </div>
                    </div>
                  </div>
                  <?php  } }?>

                  <?php
                    // 4. Release returned data
                    mysqli_free_result($result2);
                  ?>
          </div>
        </div>



        <div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
          <a class="btn-floating btn-large waves-effect waves-light red" href="#write"><i class="material-icons">add</i></a>
        </div>


